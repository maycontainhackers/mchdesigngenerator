# generator

To run this you need to install NPM. Installation instructions: https://www.npmjs.com/get-npm

For mac users for example: brew install node.


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
